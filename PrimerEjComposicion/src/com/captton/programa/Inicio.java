package com.captton.programa;
import com.captton.universidad.*;

public class Inicio {

	public static void main(String[] args) {
		
		Curso cur1 = new Curso("72322", "JAVA", 10);
		Curso cur2 = new Curso("75555", "Analisis Matematico I", 12);
		Alumno alu1 = new Alumno(38890422, "Nicolas", "Villacorta");
		Alumno alu2 = new Alumno(37532158, "Facundo", "Villafa�e");
		Profesor prof1 = new Profesor(254678123, "Matias", "Micenmach");
		Profesor prof2 = new Profesor(17597875, "Mirta", "Vazquez");
		
		cur1.setProfesor(prof1);
		cur2.setProfesor(prof2);
		
		// System.out.println("El profesor de "+cur1.getNombre()+" es "+cur1.getProfesor().getNombre());
		
		alu1.inscribirACurso(cur1);
		alu1.inscribirACurso(cur2);
		alu2.inscribirACurso(cur2);
		
		alu1.mostrarCursos();
		alu2.mostrarCursos();
		Universidad uni1 = new Universidad("UBA", "Ingenieria");
		uni1.inscribirAlumno(alu1);
		uni1.inscribirAlumno(alu2);;
		
		uni1.buscarXdni(38890422);
		
		AlumnoPos aluP1 = new AlumnoPos(37789456, "Eugenio", "Gomez");
		
		aluP1.setCuotaMensual(5000);
		
		System.out.println("El alumno de posgrado "+aluP1.getNombre()+" "+aluP1.getApellido()+" paga "+aluP1.getCuotaMensual()+" de cuota mensual.");
		
	}

}
