package com.captton.universidad;

public class Curso {

		private String codigo;
		private String nombre;
		private int cargaHoraria;
		private Profesor profesor;

		public Curso(String codigo, String nombre, int cargaHoraria) {
			super();
			this.codigo = codigo;
			this.nombre = nombre;
			this.cargaHoraria = cargaHoraria;
		}
		
		public String getCodigo() {
			return codigo;
		}
		public void setCodigo(String codigo) {
			this.codigo = codigo;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public int getCargaHoraria() {
			return cargaHoraria;
		}
		public void setCargaHoraria(int cargaHoraria) {
			this.cargaHoraria = cargaHoraria;
		}
		public Profesor getProfesor() {
			return profesor;
		}
		public void setProfesor(Profesor profesor) {
			this.profesor = profesor;
		}
		
		
		
}
