package com.captton.universidad;

import java.util.ArrayList;

public class Alumno {
	
	private int DNI;
	private String nombre;
	private String Apellido;
	private ArrayList<Curso> cursos;
	
	public Alumno(int dNI, String nombre, String apellido) {
		super();
		DNI = dNI;
		this.nombre = nombre;
		Apellido = apellido;
		this.cursos = new ArrayList<Curso>();
	}
	public int getDNI() {
		return DNI;
	}
	public void setDNI(int dNI) {
		DNI = dNI;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return Apellido;
	}
	public void setApellido(String apellido) {
		Apellido = apellido;
	}
	
	public void inscribirACurso(Curso curso) {
		this.cursos.add(curso);
	}
	
	public void mostrarCursos() {
		System.out.println("Lista de cursos inscriptos de "+this.getNombre()+" "+this.getApellido()+"\n");
		for(Curso cur: this.cursos)
		{
			System.out.println("Codigo: "+cur.getCodigo());
			System.out.println("Nombre: "+cur.getNombre());
			System.out.println("Profesor: "+cur.getProfesor().getNombre()+" "+cur.getProfesor().getApellido());
			System.out.println("---------------------------------|\n");
		}
	}
}
