package com.captton.universidad;

public class AlumnoPos extends Alumno{

	private int cuotaMensual;
	
	public AlumnoPos(int dNI, String nombre, String apellido) {
		super(dNI, nombre, apellido);
		// TODO Auto-generated constructor stub
	}

	public AlumnoPos(int dNI, String nombre, String apellido, int cuotaMensual) {
		super(dNI, nombre, apellido);
		this.cuotaMensual = cuotaMensual;
	}



	public int getCuotaMensual() {
		return cuotaMensual;
	}

	public void setCuotaMensual(int cuotaMensual) {
		this.cuotaMensual = cuotaMensual;
	}
	
	
	
}
