package com.captton.universidad;
import java.util.ArrayList;

public class Universidad {
		
		private String nombre;
		private String tipo;
		private ArrayList<Alumno> alumnos;

		public Universidad(String nom, String tip) {
			this.nombre = nom;
			this.tipo = tip;
			this.alumnos = new ArrayList<Alumno>();
		}
		
		
		
		public String getNombre() {
			return nombre;
		}



		public void setNombre(String nombre) {
			this.nombre = nombre;
		}



		public String getTipo() {
			return tipo;
		}



		public void setTipo(String tipo) {
			this.tipo = tipo;
		}



		public ArrayList<Alumno> getAlumnos() {
			return alumnos;
		}



		public void setAlumnos(ArrayList<Alumno> alumnos) {
			this.alumnos = alumnos;
		}



		public void inscribirAlumno(Alumno alum) {
			this.alumnos.add(alum);
		}
		
		public void buscarXdni(int dni) {
			Boolean encontro = false;
			for(Alumno alu: this.alumnos) {
				if(alu.getDNI()==dni) {
					encontro = true;
					System.out.println("Se encontro el DNI: "+dni);
				
				}			
			}
			
			if(!encontro)
				System.out.println("No se encontro un alumno con ese DNI.");
			/*if(this.alumnos.dni.contains(dni))
				System.out.println("Se encontro el DNI: "+dni);
			else
				System.out.println("No se encontro un alumno con ese DNI.");*/
		}
}

